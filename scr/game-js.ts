const canvas = document.createElement('canvas');
const canvasWidth:number = 900;
const canvasHeight:number = 600;
const blockSize:number = 30;
let ctx: CanvasRenderingContext2D | null;
const delay:number = 100;
// let xCoord:number = 0;
// let yCoord:number = 0;
let snakee: any;
let newDirection: string;
let allowedDirection: string | any[];
const applee:any = new apple([10, 10]);
const widthInBlock:number = canvasWidth/blockSize;
const heightInBlock:number = canvasHeight/blockSize;
let score:number;
let timeOut:number;
//this.body.overflow = "hidden";
init();

/**
 * Function qui initialise le jeu
 */
function init():void{
    canvas.width = canvasWidth;
    canvas.height = canvasHeight;
    canvas.style.border = "20px solid grey";
    canvas.style.margin = "50px auto";
    canvas.style.display = "block";
    canvas.style.backgroundColor = "#ddd"
    document.body.appendChild(canvas);
    ctx = canvas.getContext('2d');
    snakee = new Snake([[6,4], [5,4], [4,4]], "right");
    score = 0;
    refreshCanvas();
}

/**
 * function qui rafraîchi le canvas
 */
function refreshCanvas():void{
    
    snakee.advance();
    if (snakee.checkCollision()) {
        gameOver();   
    }else{
        if(snakee.isEatingApple(applee)){
            score++;
            snakee.ateApple = true;
            do{
                applee.setNewPosition();
            }
            while(applee.isOnSnake(snakee))
        }
        ctx?.clearRect(0,0,canvasWidth, canvasHeight);
        drawScore();
        snakee.draw();
        applee.draw();
        timeOut = setTimeout(refreshCanvas, delay);
    }

}
/**
 * function qui affiche le game over quand on perd
 */
function gameOver():void{
    if(ctx){
        ctx.save();
        ctx.font = "bold 70px sans-serif";
        ctx.fillStyle = "#000";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.strokeStyle = "white";
        ctx.lineWidth = 5;
        const centerX = canvasWidth / 2;
        const centerY = canvasHeight / 2;
        ctx.strokeText("Game Over", centerX, centerY - 180);
        ctx.fillText("Game Over", centerX, centerY - 180);
        ctx.font = "bold 30px sans-serif";
        ctx.strokeText("Appuyer sur Espace pour rejouer", centerX, centerY - 120);
        ctx.fillText("Appuyer sur Espace pour rejouer", centerX, centerY - 120);
        ctx.restore();
    }
}

/**
 * function redémare le jeu
 */
function restart():void{
    snakee = new Snake([[6,4], [5,4], [4,4]], "right");
    score = 0;
    clearTimeout(timeOut);
    refreshCanvas();
}
/**
 * function qui incrémente le score en temps réel
 */
function drawScore():void{
    const centerX = canvasWidth / 2;
    const centerY = canvasHeight / 2;
    if(ctx){
        ctx.save();
        ctx.font = "bold 200px sans-serif";
        ctx.fillStyle = "lightyellow";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(String(score), centerX, centerY)
        ctx.restore();
    }
}

/**
 * function qui dessine le serpent
 * @param {CanvasRenderingContext2D | null}
 * @param position - position du serpent
 */

function drawBlock(ctx:CanvasRenderingContext2D | null, position: number[]): void{
    const x = position[0] * blockSize;
    const y = position[1] * blockSize;
    ctx?.fillRect(x,y, blockSize, blockSize);
}

// class Snake {
//     body;
//     direction;
//     ateApple;
//     constructor(body,direction){
//         this.body = body;
//         this.direction = direction;
//     }

//     draw(){
        
//     }
// }

/**
 * function pour diriger le serpent
 * @param body - le corp du serpent
 * @param direction - la direction de départ du serpent
 */
function Snake(body: number[][], direction: string){
    this.body = body;
    this.direction = direction;
    this.ateApple = false;
    this.draw = function(){
        ctx?.save();
        if(ctx)
        ctx.fillStyle = "#ff0000"
        for(let i = 0; i < this.body.length; i++){
            drawBlock(ctx, this.body[i]);
        };
        ctx?.restore();

    };
    this.advance = function(){
        let nextPosition = this.body[0].slice();
        switch(this.direction){
            case "left":
                nextPosition[0] -= 1;
                break;    
            case "right":
                nextPosition[0] += 1;
                break;    
            case "down":
                nextPosition[1] += 1;
                break;    
            case "up":
                nextPosition[1] -= 1;
                break;    
        }
        this.body.unshift(nextPosition);
        if (!this.ateApple) {
            this.body.pop();    
        }
        else{
            this.ateApple = false;
        }
        // nextPosition[0] += 1;
        // this.body.unshift(nextPosition);
        // this.body.pop();
    };
    this.setDirection = function(newDirection: any)
    {
        switch(this.direction)
        {
            case "left":
            case "right":
                allowedDirection = ["up", "down"];
                break;
            case "down":
            case "up":
                allowedDirection = ["left", "right"];
                break;
            default: throw("invalid direction");
        }
        if(allowedDirection.indexOf(newDirection) > -1){
            this.direction = newDirection;
        }
    };
    this.checkCollision = function(){
        let wallCollision:boolean = false;
        let snakeCollision:boolean = false;
        const head = this.body[0];
        const rest = this.body.slice(1);
        const snakeX = head[0];
        const snakeY = head[1];
        const minX:number = 0;
        const minY:number = 0;
        const maxX = widthInBlock - 1;
        const maxY = heightInBlock - 1;
        const isNotBetweenHorizontalWalls = snakeX < minX || snakeX > maxX;
        const isNotBetweenVerticalWalls = snakeY < minY || snakeY > maxY;

        if (isNotBetweenHorizontalWalls || isNotBetweenVerticalWalls) {
            wallCollision = true;
        }
        for(let i = 0; i < rest.length; i++){
            if(snakeX === rest[i][0] && snakeY === rest[i][1]){
                snakeCollision = true;
            }
        }
        return wallCollision || snakeCollision;
    };
    this.isEatingApple = function(appleToEat){
        const head = this.body[0];
        if (head[0] === appleToEat.position[0] && head[1] === appleToEat.position[1]) {
            return true;
        }else{
            return false;
        }
    }
}

/**
 * function qui dessine la pomme
 * @param position -position de la pomme
 */
function apple(position: number[]){
    this.position = position;
    this.draw = function(){
        if(ctx){
            ctx.save();
            ctx.fillStyle = "#33cc33";
            ctx.beginPath();
            let radius = blockSize / 2;
            let x = this.position[0]*blockSize + radius;
            let y = this.position[1]*blockSize + radius;
            ctx.arc(x,y, radius, 0, Math.PI*2, true);
            ctx.fill();
            ctx.restore();

        }
    };
    this.setNewPosition = function(){
        const newX = Math.round(Math.random() * (widthInBlock - 1));
        const newY = Math.round(Math.random() * (heightInBlock - 1));
        this.position = [newX, newY]
    };
    this.isOnSnake = function(snakeToCheck){
        let isOnSnake = false;
        for(let i = 0; i < snakeToCheck.body.length; i++){
            if(this.position[0] === snakeToCheck.body[i][0] && this.position[1] === snakeToCheck.body[i][1]){
                isOnSnake = true;
            }
        }
        return isOnSnake;
    }
}

window.addEventListener("scroll", function (e){
	
	window.scrollTo(0,0);
	
}, false);

document.addEventListener('keydown', 
/**
 * evenement sur les boutons flech de direction
 * @param event -evenement à l'appuis d'un des boutton
 * @returns
 */
(event)=>{
    const key:number = event.keyCode;
    
    switch(key)
    {
        case 37:
            newDirection = "left";
            break;
        case 38:
            newDirection = "up";
            break;
        case 39:
            newDirection = "right";
            break;
        case 40:
            newDirection = "down";
            break;
            case 32:
                restart();
                return;
        default:
            return;
    }
    snakee.setDirection(newDirection);
})