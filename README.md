### TS-Game - Jeu du Serpent en TypeScript

## Description
Après plusieurs jours de réflexion, j'ai entrepris la création du légendaire jeu du Snake en utilisant TypeScript. Ce jeu classique du serpent a été élaboré en appliquant les principes de la Programmation Orientée Objet (POO) à travers l'utilisation de fonctions constructrices.

Le serpent dans le jeu gagne en longueur à chaque fois qu'il consomme une pomme. Cependant, la partie se termine si le serpent heurte le mur ou sa propre queue.

## Fonctionnalités principales
- **Croissance du serpent :** À chaque pomme consommée, le serpent gagne en longueur.
- **Gestion de la défaite :** La partie se termine si le serpent percute le mur ou se mord la queue.

## Organisation
1. **Maquettage sur Figma :** Le design visuel du jeu a été planifié à l'avance pour assurer une expérience utilisateur optimale.
2. **Utilisation de JSDoc :** Des commentaires JSDoc ont été intégrés au code pour documenter chaque fonction et rendre le code plus lisible.

## Technologies utilisées
- TypeScript
- html
- Css

## Installation
1. Clonez le dépôt `git clone https://github.com/votre_utilisateur/ts-game.git`
2. Ouvrez le fichier `index.html` dans votre navigateur web.

## Contribuer
Les contributions sont les bienvenues! Consultez le guide de contribution pour plus d'informations sur la manière de contribuer au projet TS-Game.

## Auteurs
- Chems MEZIOU
